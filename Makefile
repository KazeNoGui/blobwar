OBJSDIR = obj
INCLDIR = include
SRCDIR = src

OBJS_temp = strategy.o blobwar.o main.o font.o mouse.o image.o widget.o rollover.o button.o label.o board.o rules.o blob.o network.o bidiarray.o shmem.o
OBJS = $(addprefix $(OBJSDIR)/, $(OBJS_temp))

OBJS_launchComputation_temp = launchStrategy.o strategy.o bidiarray.o shmem.o
OBJS_launchComputation = $(addprefix $(OBJSDIR)/, $(OBJS_launchComputation_temp))

LIBS = -lSDL_image -lSDL_ttf -lm `sdl-config --libs` -lSDL_net -lpthread

CFLAGS = -Wall -Werror -O2 -g `sdl-config --cflags`  -Wno-strict-aliasing -DDEBUG -fopenmp #-pg
CC = g++

# $(sort) remove duplicate object
OBJS_ALL = $(sort $(OBJS) $(OBJS_launchComputation))

all: blobwar

blobwar: $(OBJS) launchStrategy
	$(CC) $(OBJS) $(CFLAGS) -o blobwar $(LIBS)

$(OBJS_ALL):	$(OBJSDIR)/%.o:	$(SRCDIR)/%.cc $(INCLDIR)/common.h
	$(CC) -c $< -o $@ $(CFLAGS)

launchStrategy: $(OBJS_launchComputation)
	$(CC) $(OBJS_launchComputation) $(CFLAGS) -o launchStrategy $(LIBS)

exec:
	./blobwar

clean:
	rm -f $(OBJSDIR)/*.o core blobwar launchStrategy *~ *.swp $(SRCDIR)/*~ $(SRCDIR)/*.swp $(INCLDIR)/*~ $(INCLDIR)/*.swp 

gprof:
	./launchStrategy rxxxxxxbxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxrxxxxxxb XOXXXXOXXOXXXXOXXOOXXOOXXXXXXXXXOOOXXOOOOOOOOOOOXXXXXXXXXXXXXXXX 1
	gprof launchStrategy gmon.out > analyse.txt
	vim analyse.txt

cachegrind:
	rm -rf callgrind.out*
	valgrind --tool=callgrind ./launchStrategy rxxxxxxbxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxrxxxxxxb XOXXXXOXXOXXXXOXXOOXXOOXXXXXXXXXOOOXXOOOOOOOOOOOXXXXXXXXXXXXXXXX 1
	kcachegrind callgrind.out*
