#include "../include/strategy.h"
#include <ctime>
#include <iostream>
#include <omp.h>
#include <list>
#define MAX 10000
#define MIN -10000

/*Initialisation des variables statiques */
int Strategy::profondeur = 0;
int Strategy::noeuds = 0;
move Strategy::a_jouer(0, 0, 0, 0);
unsigned int Strategy::indexVMove[50] = { 0 };
move Strategy::tvMove[50][10000000] = { { a_jouer } };

int Strategy::lower2[8] = { 0, 0, 0, 1, 2, 3, 4, 5 }; // max(0,i-2)
int Strategy::higher2[8] = { 2, 3, 4, 5, 6, 7, 7, 7 }; // min(8,i+2)
int Strategy::lower[8] = { 0, 0, 1, 2, 3, 4, 5, 6 }; // max(0,i-1)
int Strategy::higher[8] = { 1, 2, 3, 4, 5, 6, 7, 7 }; // min(8,i+1)

int Strategy::lower3[8] = { 0, 0, 0, 0, 1, 2, 3, 4 };
int Strategy::higher3[8] = { 3, 4, 5, 6, 7, 7, 7, 7 };

void Strategy::applyMove(const move& mv) {
	if (mv.nx + 2 == mv.ox || mv.nx - 2 == mv.ox || mv.ny + 2 == mv.oy
			|| mv.ny - 2 == mv.oy) {
		//Cas d'un déplacement de blob (2 cases)
		_blobs.set(mv.ox, mv.oy, -1);
	}
	//Cas d'une duplication de blob (déplacement d'une case)
	_blobs.set(mv.nx, mv.ny, _current_player);
	//Conversion des blobs autour de celui déplacé
	//On ne se souvient plus de ce qui avait été conseillé pour optimiser ce code pas beau ...
	for (Sint8 i = -1; i < 2; i++) {
		for (Sint8 j = -1; j < 2; j++) {
			if (_holes.get(mv.nx, mv.ny)) {
				continue;
			}
			if (mv.nx + i < 0) {
				continue;
			}
			if (mv.nx + i > 7) {
				continue;
			}
			if (mv.ny + j < 0) {
				continue;
			}
			if (mv.ny + j > 7) {
				continue;
			}
			if ((_blobs.get(mv.nx + i, mv.ny + j) != -1)
					&& (_blobs.get(mv.nx + i, mv.ny + j) != _current_player)) {
				_blobs.set(mv.nx + i, mv.ny + j, _current_player);
			}
		}
	}
}

//L'IA jouait très mal avec cette fonction qui aurait pu être intéressante !
//Le principal poblème de cette fonction est qu'elle n'est pas "symétrique", donc pas mal de modifications dans le code 
Sint32 Strategy::estimateCurrentScorePourrie() const {
	Sint32 currentScore = 0;
	Sint32 nb_cases = 64;
	Sint32 nb_blob1 = 0;
	Sint32 nb_blob2 = 0;
	// Principe de la fonction
	// Attribue à chacun des blobs du joueur courant un nombre de points
	// En fonction de l'hostilité de son voisinage. (ségrégation)
	for (int i = 0; i < 8; i++) {
		for (int j = 0; j < 8; j++) {
			if (_holes.get(i, j)) {
				nb_cases--;
			}
			//Case contenant un blob allié
			if (_blobs.get(i, j) == _current_player) {
				nb_blob1++;
				currentScore++;
				Uint8 newx;
				Uint8 newy;
				Uint8 EN1 = 0;
				Uint8 EN2 = 0;
				Uint8 EM1 = 0;
				// Petite couronne (voisins directs)
				for (newx = lower[i]; newx <= higher[i]; newx++) {
					for (newy = lower[j]; newy <= higher[j]; newy++) {
						if (_blobs.get(newx, newy) == -1) {
							EM1++;
							continue;
						}
						if (_blobs.get(newx, newy) == 1 - _current_player) {
							EN1++;
						}
					}
				}
				if (EM1 != 0) { // si case libre directement à côté du blob !DANGER!
					if (EN1 == 0) { //EN1 = Ennemis dans la petite couronne
						// On regarde si il y a des ennemis pouvant le croquer
						// Grande couronne (à deux cases).
						for (newx = lower2[i]; newx <= higher2[i]; newx++) {
							if (_blobs.get(newx, lower2[j])
									== 1 - _current_player) {
								EN2++;
							}
							if (_blobs.get(newx, higher2[j])
									== 1 - _current_player) {
								EN2++;
							}
						}
						for (newy = lower[j]; newy <= higher[j]; newy++) {
							if (_blobs.get(lower2[i], newy)
									== 1 - _current_player) {
								EN2++;
							}
							if (_blobs.get(higher2[i], newy)
									== 1 - _current_player) {
								EN2++;
							}
						}
						if (EN2 != 0) { // On décrémente si un ennemi est dans la grande couronne
							currentScore--;
						} else {
							// Tres Grande Couronne pour être sûr ! (on garde EN2 car = 0)
							// Très couteux en temps mais bon comme on descend pas très loin ...
							for (newx = lower3[i]; newx <= higher3[i]; newx++) {
								if (_blobs.get(newx, lower3[j])
										== 1 - _current_player) {
									EN2++;
								}
								if (_blobs.get(newx, higher3[j])
										== 1 - _current_player) {
									EN2++;
								}
							}
							for (newy = lower2[j]; newy <= higher2[j]; newy++) {
								if (_blobs.get(lower3[i], newy)
										== 1 - _current_player) {
									EN2++;
								}
								if (_blobs.get(higher3[i], newy)
										== 1 - _current_player) {
									EN2++;
								}
							}
							if (EN2 != 0) { // On décrémente si un ennemi est dans la grande couronne
								currentScore--;
							} else {
								currentScore += 3; // BONUS: Il sera en vie au tour suivant ! et pas d'ennemis en vue.
							}
						}
					} else {
						currentScore--; // On décrémente le score si un ennemi est présent dans la petite couronne
					}
				} else {
					currentScore += 2; // BONUS: Il sera en vie au tour suivant !
				}
			}
			//Case contenant un blob ennemi
			else if (_blobs.get(i, j) == 1 - _current_player) {
				nb_blob2++;
				currentScore--;
			}
		}
	}
	if (nb_cases == nb_blob1 + nb_blob2) {
		if (nb_blob1 > nb_blob2) {
			return (MAX);
		} else {
			return (MIN);
		}
	}
	return currentScore;
}

// Retour à la fonction classique, sans l'optimisation (changement de la valeur des blobs +1/-1
Sint32 Strategy::estimateCurrentScore() const {
	Sint32 currentScore = 0;
	Sint32 nb_cases = 64;
	Sint32 nb_blob1 = 0;
	Sint32 nb_blob2 = 0;
	for (int i = 0; i < 8; i++) {
		for (int j = 0; j < 8; j++) {
			if (_holes.get(i, j)) {
				nb_cases--;
			}
			//Case contenant un blob allié
			if (_blobs.get(i, j) == _current_player) {
				nb_blob1++;
				currentScore++;
			}
			//Case contenant un blob ennemi
			else if (_blobs.get(i, j) == 1 - _current_player) {
				nb_blob2++;
				currentScore--;
			}
		}
	}
	if (nb_cases == nb_blob1 + nb_blob2) {
		if (nb_blob1 > nb_blob2) {
			return (MAX);
		} else {
			return (MIN);
		}
	}
	return currentScore;
}

void Strategy::computeValidMoves() {
	Uint8 x = 0;
	Uint8 y = 0;
	for (x = 0; x < 8; x++) {
		for (y = 0; y < 8; y++) {
			//Recherche des blobs du joueur actuel
			if (_blobs.get(x, y) == (int) _current_player) {
				Uint8 newx;
				Uint8 newy;

				//Insertion classique des valid moves, pas d'ordre précis dans ce cas
				/*
				 for (newx = lower2[x]; newx <= higher2[x];
				 newx++) {
				 for (newy = lower2[y]; newy <= higher2[y];
				 newy++) {
				 //Trou
				 if (_holes.get(newx, newy))
				 continue;
				 //Case vide
				 if (_blobs.get(newx, newy) == -1) {
				 move newMove(x, y, newx, newy);
				 tvMove[indice_thread][indexVMove[indice_thread]] = newMove;
				 indexVMove[indice_thread]++;
				 }
				 }
				 }
				 */

				//Insertion des valid moves dans un ordre précis 
				//(amélioration de l'élagage si les coups où les blobs se multiplient sont mieux, on peut partir de ce principe)
				// Petite couronne (voisins directs)
				for (newx = lower[x]; newx <= higher[x]; newx++) {
					for (newy = lower[y]; newy <= higher[y]; newy++) {
						//Trou        //                                  ###
						if (_holes.get(newx, newy))  //                   #*#
							continue;  //                                 ###
						//Case vide
						if (_blobs.get(newx, newy) == -1) {
							move newMove(x, y, newx, newy);
							tvMove[indice_thread][indexVMove[indice_thread]] =
									newMove;
							indexVMove[indice_thread]++;
						}
					}
				}
				// Grande Couronne
				for (newx = lower2[x]; newx <= higher2[x]; newx++) { //-----------------
					//Trou                                            //                |
					if (_holes.get(newx, higher2[y]))        //                |
						continue;                            //                |
					//Case vide                                       //                |
					if (_blobs.get(newx, higher2[y]) == -1) { //                  |-> #####
						move newMove(x, y, newx, higher2[y]); //                  |    ¤¤¤
						tvMove[indice_thread][indexVMove[indice_thread]] =
								newMove; //  |    ¤*¤
						indexVMove[indice_thread]++; //                   |    ¤¤¤
					} //Trou                                        //   -----------------   ##### <-----
					if (_holes.get(newx, lower2[y]))   //-----                 |
						continue;                      //    |                 |
					//Case vide                                                //    |                 |
					if (_blobs.get(newx, lower2[y]) == -1) { //    |------------------
						move newMove(x, y, newx, lower2[y]); //    |
						tvMove[indice_thread][indexVMove[indice_thread]] =
								newMove;                   //|
						indexVMove[indice_thread]++;                     //-----
					}
				}
				for (newy = lower[y]; newy <= higher[y]; newy++) { //-----------------
					//Trou                                          //                |
					if (_holes.get(lower2[x], newy))  //                |-------
						continue;                     //                |      |
					//Case vide                                     //                |      v
					if (_blobs.get(lower2[x], newy) == -1) { //                |      ¤¤¤¤¤
						move newMove(x, y, lower2[x], newy); //                |      #¤¤¤#
						tvMove[indice_thread][indexVMove[indice_thread]] =
								newMove; //|      #¤*¤#
						indexVMove[indice_thread]++; //                |      #¤¤¤#
					} //Trou                                         //-----------------      ¤¤¤¤¤
					if (_holes.get(higher2[x], newy))        //-----           ^
						continue;                            //    |           |
					//Case vide                                                //    |           |
					if (_blobs.get(higher2[x], newy) == -1) { //    |------------
						move newMove(x, y, higher2[x], newy);           //    |
						tvMove[indice_thread][indexVMove[indice_thread]] =
								newMove;                  //|
						indexVMove[indice_thread]++;                     //-----
					}
				}
			}

		}
	}
}

void Strategy::computeBestMove() {
	clock_t t_begin, t_end;
	//Itération sur la profondeur d'exploration de l'arbres des possibilités
	for (int i = 1; i < 101; i++) { //toutes les prof
		//for (int i = 4; i<5; i++){ // prof = 4
		profondeur = i;
		for (int k = 0; k < 50; k++) {
			indexVMove[k] = 0;
		}
		t_begin = clock();
		noeuds = 0;
		//MinMax(*this, profondeur);
		//MinMax_P(*this, profondeur);
		AlphaBeta(*this, MIN, MAX, profondeur);
		//AlphaBeta_P(*this, MIN, MAX, profondeur);
		_saveBestMove(a_jouer);
		t_end = clock();
		std::cout << "profondeur " << i << " exécuté en " << t_end - t_begin
				<< " tours de clock" << endl;
		std::cout << "nombre de noeuds: " << noeuds << endl;
	}
	std::cout << std::endl;
}

/* Change le joueur courant */
void Strategy::switchPlayer() {
	_current_player = 1 - _current_player;
}

Sint32 Strategy::MinMax(Strategy s, int p) {
	if (p == 0) {
		return s.estimateCurrentScore();
	} else {
		unsigned int indexDebut;
		unsigned int indexFin;
		indexDebut = indexVMove[s.getIndice_thread()];
		s.computeValidMoves();
		indexFin = indexVMove[s.getIndice_thread()];
		Sint32 score = MIN;
		if (indexDebut == indexFin) {
			Strategy fils(s);
			fils.switchPlayer();
			score = -MinMax(fils, p - 1);
		}
		for (unsigned int i = indexDebut; i < indexFin; i++) {
			noeuds++;
			Strategy fils(s);
			fils.applyMove(tvMove[s.getIndice_thread()][i]);
			fils.switchPlayer();
			Sint32 res = -MinMax(fils, p - 1);
			if (res > score) {
				score = res;
				if (p == profondeur) {
					a_jouer = tvMove[s.getIndice_thread()][i];
				}
			}
		}
		return score;
	}
}

Sint32 Strategy::MinMax_P(Strategy s, int p) {
	s.setIndice_thread(0);
	unsigned int indexDebut;
	unsigned int indexFin;
	indexDebut = indexVMove[indice_thread];
	s.computeValidMoves();
	indexFin = indexVMove[indice_thread];
	Sint32 score = MIN;
	if (indexDebut == indexFin) {
		Strategy fils(s);
		fils.switchPlayer();
		score = -MinMax(fils, p - 1);
	}
	int copie_indice_thread = indice_thread;
#pragma omp parallel for schedule(dynamic) reduction(+:copie_indice_thread)
	for (unsigned int i = indexDebut; i < indexFin; i++) {
		copie_indice_thread++;
		Strategy fils(s);
		s.setIndice_thread(copie_indice_thread);
		fils.applyMove(tvMove[indice_thread][i]);
		fils.switchPlayer();
		Sint32 res = -MinMax(fils, p - 1);
#pragma omp critical
		{
			if (res > score) {
				score = res;
				if (p == profondeur) {
					a_jouer = tvMove[indice_thread][i];
				}
			}
		}
	}
	return score;
}

Sint32 Strategy::AlphaBeta(Strategy s, Sint32 alpha, Sint32 beta, int p) {
	if (p == 0) {
		return s.estimateCurrentScore();
	} else {
		unsigned int indexDebut;
		unsigned int indexFin;
		indexDebut = indexVMove[s.getIndice_thread()];
		s.computeValidMoves();
		indexFin = indexVMove[s.getIndice_thread()];
		Sint32 score = MIN;
		if (indexDebut == indexFin) {
			Strategy fils(s);
			fils.switchPlayer();
			score = -AlphaBeta(fils, -beta, -alpha, p - 1);
		}
		for (unsigned int i = indexDebut; i < indexFin; i++) {
			noeuds++;
			Strategy fils(s);
			fils.applyMove(tvMove[s.getIndice_thread()][i]);
			fils.switchPlayer();
			Sint32 res = -AlphaBeta(fils, -beta, -alpha, p - 1);
			if (res > score) {
				score = res;
				if (p == profondeur) {
					a_jouer = tvMove[s.getIndice_thread()][i];
				}
				if (res > alpha) {
					alpha = score;
					if (alpha >= beta) {
						return score;
					}
				}
			}
		}
		return score;
	}
}

Sint32 Strategy::AlphaBeta_P(Strategy s, Sint32 alpha, Sint32 beta, int p) {
	s.setIndice_thread(0);
	unsigned int indexDebut = indexVMove[indice_thread];
	s.computeValidMoves();
	unsigned int indexFin = indexVMove[indice_thread];
	Sint32 score = MIN;
	std::list<Sint32> retour;
	if (indexDebut == indexFin) {
		Strategy fils(s);
		fils.switchPlayer();
		score = -AlphaBeta(fils, -beta, -alpha, p - 1);
	}
	int copie_indice_thread = indice_thread;
	//#pragma omp parallel for schedule(dynamic) reduction(+:copie_indice_thread)
	omp_set_dynamic(0);     // Explicitly disable dynamic teams
#pragma omp parallel for reduction(+:copie_indice_thread) num_threads(16)
	for (unsigned int i = indexDebut; i < indexFin; i++) {
		copie_indice_thread++;
		Strategy fils(s);
		s.setIndice_thread(copie_indice_thread);
		fils.applyMove(tvMove[indice_thread][i]);
		fils.switchPlayer();
		Sint32 res = -AlphaBeta(fils, -beta, -alpha, p - 1);
#pragma omp critical
		{
			if (res > score) {
				score = res;
				if (p == profondeur) {
					a_jouer = tvMove[indice_thread][i];
				}
				if (res > alpha) {
					alpha = score;
					if (alpha >= beta) {
						retour.push_back(score);
						//return score;
					}
				}
			}
		}
	}
	if (!retour.empty()) {
		retour.sort();
		score = retour.back();
	}
	return score;
}
