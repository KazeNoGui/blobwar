#ifndef __STRATEGY_H
#define __STRATEGY_H

#include "common.h"
#include "bidiarray.h"
#include "move.h"



class Strategy {

private:
    //! array containing all blobs on the board
    bidiarray<Sint16> _blobs;
    //! an array of booleans indicating for each cell whether it is a hole or not.
    const bidiarray<bool>& _holes;
    //! Current player
    Uint16 _current_player;
    
    //! Call this function to save your best move.
    //! Multiple call can be done each turn,
    //! Only the last move saved will be used.
    void (*_saveBestMove)(move&);
    int indice_thread;

public:

    static int lower[];
    static int higher[];
    static int lower2[];
    static int higher2[];
    static int lower3[];
    static int higher3[];

    //Permet de fixer la profondeur d'exploration pour l'algo minmax ou alphabeta
    static int profondeur;
    static int noeuds; 

    //Permet de stocker la prochain mouvement trouvé par l'algo minmax ou alphabeta (pour une profondeur fixée)
    static move a_jouer; 

    static unsigned int indexVMove[];

    static move tvMove[][10000000];

    void switchPlayer();
    
        // Constructor from a current situation
    Strategy (bidiarray<Sint16>& blobs, 
              const bidiarray<bool>& holes,
              const Uint16 current_player,
              void (*saveBestMove)(move&))
            : _blobs(blobs),_holes(holes), _current_player(current_player), _saveBestMove(saveBestMove), indice_thread(0)
        {}
    
        // Copy constructor
    Strategy (const Strategy& St)
            : _blobs(St._blobs), _holes(St._holes),_current_player(St._current_player), _saveBestMove(St._saveBestMove), indice_thread(St.getIndice_thread())
        {}
    
        // Destructor
    ~Strategy() {}
    
        /** 
         * Apply a move to the current state of blobs
         * Assumes that the move is valid
         */
    void applyMove (const move& mv);

        /**
         * Compute the vector containing every possible moves
         */
    void computeValidMoves ();

        /**
         * Estimate the score of the current state of the game
         */
    Sint32 estimateCurrentScore () const;

        /**
         * Find the best move.
         */
    void computeBestMove ();

    void setIndice_thread(int i){
	indice_thread = i;
    }

    int getIndice_thread() const{
	return indice_thread;
    }

    Sint32 estimateCurrentScorePourrie () const;
    Sint32 MinMax(Strategy s, int profondeur);
    Sint32 MinMax_P(Strategy s, int profondeur);
    Sint32 AlphaBeta(Strategy s, Sint32 alpha, Sint32 beta, int profondeur);
    Sint32 AlphaBeta_P(Strategy s, Sint32 alpha, Sint32 beta, int profondeur);
    
};

#endif
